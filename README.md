# Animal Welfare Bot

A [Telegram](https://telegram.org/) bot that implements a basic animal cruelty reporting system, based on the [ASPCA](https://www.aspca.org) approach.

This bot is a demonstration of the Java [Talks](https://gitlab.com/gllona/talks) Telegram chatbot framework.

There are abundant references about [Telegram Bots](https://core.telegram.org/bots).

## Disclaimer

I'm not affiliated with ASPCA. I used ASPCA's website information as a reference for this bot's conversational interactions design.

## How it works

A Telegram user can access the bot and report an animal cruelty event. They would fill a questionnaire about the event. The report will be sent to other bot users which have the `OPERATOR` role. After that, operators and reporting users can communicate using user-to-user Telegram interactions, or an embedded communication system that preserves the anonymity of the operators.

The conversational interactions and questionnaires are localised. When the user enters the bot, they will select their location (place) from a list. Currently the only one implemented place is `PLANET EARTH`. After the selection, the user will be directed to place-specific interactions. This means that the bot can be extended to support multiple places where animal cruelty is reported in different ways.

See [this script file](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/resources/script.txt) for an outline of the bot's conversational interactions.

## Structure

### User roles

Each user can be granted a `ROLE` with a specific `SCOPE`. In this implementation the scopes are taken from the `Place` enum. The available roles are:

* `ADMIN`: There can be one of more admin users. Those admin users have access to the `/admin` command, which allows accessing authorization functions: grant, revoke and list granted authorizations.
* `OPERATOR`: These users handle communication with regular bot users. When a regular bot user initiates a communication with the bot owners, the operators receive their messages and can answer to them.
* `USER`: A bot user that has not be granted any authorization defaults to this role.

Your code logic can ask for privileges associated with a specific scope. For example, you can ask if a user is an `OPERATOR` with `PLANET EARTH` scope, an `ADMIN` with `PLANET EARTH` scope, or an `ADMIN` with generic scope (meaning that their scope will be a `null` in code and database). 

### Talks

Each `Talk` subclass implements a set of bot <--> user interactions. This project comprises the following Talks: 

* `WelcomeTalk`: interactions before the user selects their place
* `EarthTalk`: initial interactions after the user selects `PLANET EARTH`
* `EarthSurveyTalk`: animal cruelty events questionnaire for `PLANET EARTH`
* `SurveyTalk`: generic implementation of surveys/questionnaires interactions
* `AdminTalk`: restricted administrative functions menu
* `AuthTalk`: authorizations function interactions (assign roles to users)
* `CommunicatorTalk`: user <--> operator communication interactions

Every `Talk` subclass is registered in the `Talks` enum.

### Subpackages

The project is structured in these packages:

* `org.animal`: the base package includes the `App` main class, some config classes, the `Place` enum, the `Talks` enum and some utility classes
* `org.animal.intro`: home of the `WelcomeTalk`
* `org.animal.earth`: home of the `EarthTalk` as well as the `PLANET EARTH` questionnaire metadata and callbacks
* `org.animal.admin`: Talks, enums and classes related to administrative functions. Includes database access classes for the entity `User`
* `org.animal.communicator`: Talk and services for the user <--> operator function
* `org.animal.survey`: generic Talk for the questionnaire-based interactions, and support classes
* `org.animal.geoloc`: a service which provides forward geolocation, based on a `LocationIQ` client. Not used in this project's interactions

## What you need

* Oracle Java 11 JDK
* MySql 8 Server

Code level is Java 8. You can try with OpenJDK 8 or 11 as well as MySql 5.6 or MariaDB.

## How to run

* Clone the repo
* `cd` to directory
* Register a new Telegram bot (see below)
* Create a MySql database and optionally a MySql user with proper rights to the database
* Create your `config.properties` file based on the example (see below)
* `cd app && ln -s ../config.properties config.properties && cd ..`

### In local environment

* `./gradlew :app:run --no-daemon`
* Or run the `App` class in your favorite IDE

### In production environment

*  `./gradlew clean shadowJar`
* `java -jar app/build/libs/app-all.jar`

## Configure the bot

You will need to register a Telegram bot, which is associated with your Telegram account. For each account you can register a maximum of 20 bots.

To register a Telegram bot use [BotFather](https://t.me/BotFather). Set the bot name, description and get the `API Token`

Previous to the first run, you have to create a `config.properties` configuration file. Make a copy of the provided `config.properties.EXAMPLE` and edit it as follows:

* Database settings:
  * Set the MySql database name in `FLYWAY_URL` and `DB_DATABASE` 
  * Set the MySql user in `FLYWAY_USER` and `DB_USERNAME`
  * Set the MySql password in `FLYWAY_PASSWORD` and `DB_PASSWORD`
  * If you get trouble working with buttons with emoji, try setting `ENCODE_EMOJI=1`
* Telegram bot settings:
  * Set the bot name (finished in `bot`, `Bot`, etc) in `BOT_NAME`
  * Set the bot API Token in `TELEGRAM_API_KEY`
* Security settings:
  * Set a first-time administrator PIN in `MAGIC_CODE`. Example: `8888`
* Logging settings:
  * Set `SESSIONS_TO_LOG` to `*` to log all interactions. Or set it to a comma-separated list of Telegram chat ID's
  (Note: chat ID's appear `[between_brackets]` in all logged interactions)
* Other settings:
  * Set `FALLBACK_RESPONSE` to the text to be used as a default response to the user when there are not matching interactions defined in code
  * If you modify the bot to use the `GeolocService`, you will need a [LocationIQ](https://locationiq.com) API Key. Set it in `LOCATION_IQ_API_KEY`

## Run the bot

The first time you run the bot, it will prompt all users for the first-time administrator PIN. The first user that enters it correctly will become an `ADMIN` and will have access to the `/admin` command.

Once you enter the first-time administrator PIN, you can tap on `/start` to access the user-level interactions.

To complete the configuration, use the `/admin` command to assign some users (for example, yourself) the `OPERATOR` role for both the scopes `PLANET EARTH` and `All`.

Note that after a user has been assigned an administrative privilege, they should restart the bot with `/start` to get the permissions applied.

## Additional information

Check the [Talks framework](https://gitlab.com/gllona/talks) website.

## Author

Developed by [Gorka Llona](http://desarrolladores.logicos.org/gorka).
