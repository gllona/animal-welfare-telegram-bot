package org.animal.intro;

import java.util.List;
import java.util.Optional;
import org.animal.Place;
import org.animal.Triggers;
import org.animal.Utils;
import org.animal.admin.AuthService;
import org.animal.geoloc.GeolocService;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class WelcomeTalk extends Talk {

    private final AuthService authService = AuthService.getInstance(talksConfig);
    private final GeolocService geolocService = GeolocService.getInstance(talksConfig);

    public WelcomeTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("intro.WelcomeTalk::start",
                InputTrigger.ofRegex("/start( .*)?"))
        );
    }

    public TalkResponse start(TalkRequest request) {
        Optional<TalkResponse> initialization = authService.firstTimeRoute(request.getChatId());
        if (initialization.isPresent()) {
            return initialization.get();
        }

        authService.fillInUsers(request);

        List<String> matches = request.getMatch().getRegexMatches();
        String arguments = matches.size() == 1 ? null : matches.get(1);

        if (arguments != null) {
            return startWithArguments(request, arguments);
        }

        resetContext(request);

        return welfare(request);
    }

    public TalkResponse welfare(TalkRequest request) {
        String animalPhoto = request.getChatId() % 2 == 1 ? "dog_aspca.jpg" : "cat_aspca.jpg";
        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                Utils.fileFrom(animalPhoto)
            )
        );

        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "👋 Hi! This bot allows you to report animal cruelty.\n"
                + "\n"
                + "Where do you live?"
        );

        for (Place place : Place.values()) {
            response.withButton("intro.WelcomeTalk::processPlace", place.getStdName());
        }
        response.withRegex("intro.WelcomeTalk::processPlace", Triggers.NOT_A_COMMAND);

        return response;
    }

    public TalkResponse processPlace(TalkRequest request) {
        String placeStr = request.getMessage().text();
        Place place = Place.from(placeStr);

        if (place == null) {
            return notSupportedPlace(request);
        }

        getContext().set(request.getChatId(), "PLACE", place);

        return talkForClass(place.getTalkClass())
            .menu(request);
    }

    private TalkResponse notSupportedPlace(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Your place is not currently supported.\n"
                + "\n"
                + "* To write us tap on /here\n"
                + "* To restart the bot tap on /start"
            ).withString("communicator.CommunicatorTalk::writeToOperator", "/here");
    }

    private TalkResponse startWithArguments(TalkRequest request, String arguments) {
        return TalkResponse.ofText(
            request.getChatId(),
            "/start with arguments is not supported yet"
        );
    }

    private void resetContext(TalkRequest request) {
        talksConfig.getTalkManager().resetTalkContexts(request.getChatId());
    }
}
