package org.animal.communicator;

import com.pengrad.telegrambot.model.Chat;
import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.animal.Place;
import org.animal.Triggers;
import org.animal.Utils;
import org.animal.admin.AuthService;

public class CommunicatorTalk extends Talk {

    private final AuthService authService = AuthService.getInstance(talksConfig);
    private final CommunicatorService communicatorService = CommunicatorService.getInstance(talksConfig);

    public CommunicatorTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("WelcomeTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("communicator.CommunicatorTalk::write",
                InputTrigger.ofRegex("/answer_?([0-9]+)?_?([A-Za-z0-9_]+)?"))
        );
    }

    public TalkResponse write(TalkRequest request) {
        String targetChatIdStr = request.getMatch().getRegexMatches().get(1);
        String targetTgUserId = request.getMatch().getRegexMatches().get(2);

        if (targetChatIdStr != null) {
            if (authService.userCanCommunicateWithOtherUsers(request.getChatId())) {
                long targetChatId = Long.parseLong(targetChatIdStr);
                return writeToUser(request, targetChatId, targetTgUserId);
            } else {
                return unauthorizedOperation(request);
            }
        } else {
            return writeToOperator(request);
        }
    }

    private TalkResponse writeToUser(TalkRequest request, long targetChatId, String targetTgUserId) {
        getContext().set(request.getChatId(), "TARGET_CHAT_ID", targetChatId);
        getContext().set(request.getChatId(), "TARGET_TG_USER_ID", targetTgUserId);

        return TalkResponse.ofText(
            request.getChatId(),
            "Enter your answer to the user in a single paragraph:"
        ).withRegex("communicator.CommunicatorTalk::doWriteToUser", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doWriteToUser(TalkRequest request) {
        Place place = Place.from(
            getContext().getString(request.getChatId(), "PLACE")
        );
        Long targetChatId = getContext().getLong(request.getChatId(), "TARGET_CHAT_ID");
        String targetTgUserIdVarValue = getContext().getString(request.getChatId(), "TARGET_TG_USER_ID");
        String targetTgUserId = targetTgUserIdVarValue == null ? null : "@" + targetTgUserIdVarValue;
        String targetScope = getContext().getString(targetChatId, "PLACE");
        String operatorTgUserId = "@" + request.getMessage().chat().username();

        communicatorService.sendToOperatorsBoxed(request, place, Optional.of(request.getChatId()),
            "OPERATOR WROTE TO USER"
                + (targetScope == null ? "" : " (" + targetScope + ")"),
            request.getMessage().text(),
            "END OF MESSAGE",
            "To answer: answer_" + targetChatId + (targetTgUserIdVarValue == null ? "" : "_" + targetTgUserIdVarValue)
                + (targetTgUserId == null ? "" : " o " + targetTgUserId)
                + ". Or chat with the Operator: " + operatorTgUserId
        );

        communicatorService.sendToUser(
            request,
            targetChatId,
            "MESSAGE FROM OPERATOR",
            request.getMessage().text(),
            "END OF MESSAGE",
            "To answer the Operator tap on /answer",
            true
        );

        return TalkResponse.ofText(
            request.getChatId(),
            "The answer has been sent to the user.\n"
                + "\n"
                + "You can send them another message with /answer_" + targetChatId
        );
    }

    public TalkResponse writeToOperator(TalkRequest request) {
        String scopeStr = getContext().getString(request.getChatId(), "PLACE");
        Place place = Place.from(scopeStr);

        return TalkResponse.ofText(
            request.getChatId(),
            "📥 Here you can send us a question or comment.\n"
                + "\n"
                + "Please write your question in a single paragraph.\n"
                + "\n"
                + "You can also go back to main /menu"
        ).withString(Utils.menuClassAndMethod(place), "/menu")
         .withRegex("communicator.CommunicatorTalk::doWriteToOperator", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doWriteToOperator(TalkRequest request) {
        String scopeStr = getContext().getString(request.getChatId(), "PLACE");
        Place place = Place.from(scopeStr);

        Chat chat = request.getMessage().chat();
        String tgUsername = chat.username() == null ? null : "@" + chat.username();
        String tgFullName = (chat.firstName() + " " + (chat.lastName() == null ? "" : chat.lastName())).trim();

        communicatorService.sendToOperatorsBoxed(request, place,
            "MESSAGE FROM USER\n"
                + tgFullName
                + " (" + scopeStr + ")\n"
                + "Has sent the following question or comment:",
            request.getMessage().text(),
            "END OF MESSAGE",
            "To answer: /answer_" + request.getChatId() + (chat.username() == null ? "" : "_" + chat.username())
                + (tgUsername == null ? "" : " o " + tgUsername)
        );

        return TalkResponse.ofText(
            request.getChatId(),
            "Thank you. If you sent us a question, one of us will contact you soon.\n"
                + "\n"
                + "Pay attention to Telegram!\n"
                + "\n"
                + "* Go back to main /menu"
        ).withString(Utils.menuClassAndMethod(place), "/menu");
    }

    private TalkResponse unauthorizedOperation(TalkRequest request) {
        Place place = Place.from(
            getContext().getString(request.getChatId(), "PLACE")
        );

        return TalkResponse.ofText(
            request.getChatId(),
            "You don't have enough privileges for this operation.\n"
                + "\n"
                + "* Go back to main /menu"
        ).withString(Utils.menuClassAndMethod(place), "/menu");
    }
}
