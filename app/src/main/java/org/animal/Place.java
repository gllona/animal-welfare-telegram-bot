package org.animal;

import java.util.stream.Stream;
import lombok.Getter;
import org.animal.earth.EarthTalk;

public enum Place {

    EARTH("Planet Earth", EarthTalk.class);

    @Getter private final String stdName;
    @Getter private final Class<? extends PlaceMainTalk> talkClass;

    Place(String stdName, Class<? extends PlaceMainTalk> talkClass) {
        this.stdName = stdName;
        this.talkClass = talkClass;
    }

    public static Place from(String name) {
        String stdName = Utils.normalizeInternationalCharacters(name);
        Place place = Stream.of(values())
                            .filter(c -> c.stdName.equalsIgnoreCase(stdName))
                            .findFirst()
                            .orElse(null);
        return place;
    }
}
