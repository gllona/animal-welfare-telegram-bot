package org.animal;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.engine.Talk;
import org.animal.admin.AdminTalk;
import org.animal.admin.AuthTalk;
import org.animal.communicator.CommunicatorTalk;
import org.animal.intro.WelcomeTalk;
import org.animal.earth.EarthTalk;
import org.animal.earth.EarthSurveyTalk;
import org.animal.survey.SurveyTalk;

@AllArgsConstructor
@Getter
public enum Talks implements TalkType {

    // Important: All enum elements should have the same name and capitalization as the class name
    WelcomeTalk(WelcomeTalk.class),
    WorldTalk(EarthTalk.class),
    WorldSurveyTalk(EarthSurveyTalk.class),
    AdminTalk(AdminTalk.class),
    AuthTalk(AuthTalk.class),
    CommunicatorTalk(CommunicatorTalk.class),
    SurveyTalk(SurveyTalk.class);

    private Class<? extends Talk> talkClass;

    public static List<TalkType> getAllTalkTypes() {
        return Arrays.stream(values())
            .map(t -> (TalkType) t)
            .collect(Collectors.toList());
    }

    @Override
    public Class<? extends Talk> getTalkClass() {
        return talkClass;
    }

    @Override
    public String getName() {
        return talkClass.getSimpleName();
    }
}
