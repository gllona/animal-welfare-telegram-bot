package org.animal.admin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.animal.Place;

public class Scope {

    public static List<String> getAll() {
        return Stream.of(Place.values())
            .map(Enum::name)
            .collect(Collectors.toList());
    }

    public static boolean isValid(String scope) {
        if (scope == null) {
            return true;
        }
        try {
            Place.valueOf(scope);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
