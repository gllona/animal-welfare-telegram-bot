package org.animal.admin;

import java.util.Optional;
import java.util.stream.Collectors;
import org.animal.AppSetting;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.animal.Triggers;

public class AuthTalk extends Talk {

    private AuthService authService = AuthService.getInstance(talksConfig);

    public AuthTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("WelcomeTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    public TalkResponse firstTimeCode(TalkRequest request) {
        int number = Integer.parseInt(request.getMessage().text());

        if (number == getMagicCode()) {
            authService.firstTimeMagicCodeGot(request);

            return TalkResponse.ofText(
                request.getChatId(),
                "Got it!\n"
                    + "\n"
                    + "Now tap on /start");
        }

        return TalkResponse.ofText(request.getChatId(), "Retry.")
            .withRegex("admin.AuthTalk::firstTimeCode", Triggers.FOUR_DIGITS);
    }

    public TalkResponse authorizations(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Grant or revoke permits:"
           ).withButton("admin.AuthTalk::list", "List")
            .withButton("admin.AuthTalk::grant", "Grant")
            .withButton("admin.AuthTalk::revoke", "Revoke")
            .withButton("admin.AdminTalk::admin", "Go back");
    }

    public TalkResponse list(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
            request.getChatId(),
            authService.findAllPrivilegedUsers().stream().map(User::prettyToString).collect(Collectors.joining("\n\n"))
           ).withButton("admin.AuthTalk::authorizations", "Continue");
    }

    public TalkResponse grant(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        getContext().set(request.getChatId(), "ADMIN_GRANTREVOKE", "GRANT");

        return grantRevokeStart(request);
    }

    public TalkResponse revoke(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        getContext().set(request.getChatId(), "ADMIN_GRANTREVOKE", "REVOKE");

        return grantRevokeStart(request);
    }

    private TalkResponse grantRevokeStart(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Enter Telegram username:"
           ).withRegex("admin.AuthTalk::grantRevokeProcessUsername", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse grantRevokeProcessUsername(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        String enteredText = request.getMessage().text();
        String username = enteredText.startsWith("@") ? enteredText.substring(1) : enteredText;

        getContext().set(request.getChatId(), "ADMIN_GRANT_USERNAME", username);

        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "Select Role:"
        );
        for (Role role : Role.privileged()) {
            response.withButton("admin.AuthTalk::grantRevokeProcessRole", role.name());
        }
        return response;
    }

    public TalkResponse grantRevokeProcessRole(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        Role role = Role.from(request.getMessage().text());
        if (role == null) {
            return invalidEntryResponse(request);
        }

        getContext().set(request.getChatId(), "ADMIN_GRANT_ROLE", role.toString());

        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "Select Scope:"
           ).withButton("admin.AuthTalk::grantRevokeProcessScope", "All");
        for (String scope : Scope.getAll()) {
            response.withButton("admin.AuthTalk::grantRevokeProcessScope", scope);
        }
        return response;
    }

    public TalkResponse grantRevokeProcessScope(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        String scope = "All".equals(request.getMessage().text()) ?
            null :
            request.getMessage().text();

        if (! Scope.isValid(scope)) {
            return invalidEntryResponse(request);
        }

        String operation = getContext().getString(request.getChatId(), "ADMIN_GRANTREVOKE");

        if ("GRANT".equals(operation)) {
            authService.save(
                getContext().getString(request.getChatId(), "ADMIN_GRANT_USERNAME"),
                Role.valueOf(getContext().getString(request.getChatId(), "ADMIN_GRANT_ROLE")),
                scope
            );
        } else if ("REVOKE".equals(operation)) {
            authService.delete(
                getContext().getString(request.getChatId(), "ADMIN_GRANT_USERNAME"),
                Role.valueOf(getContext().getString(request.getChatId(), "ADMIN_GRANT_ROLE")),
                scope
            );
        } else {
            throw new RuntimeException("invalid auth operation " + operation);
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Operation completed."
           ).withButton("admin.AuthTalk::authorizations", "Continue");
    }

    private TalkResponse invalidEntryResponse(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Invalid entry."
        ).withPreserveHandlers();
    }

    private TalkResponse unauthorizedResponse(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Not authorized."
        ).withPreserveHandlers();
    }

    private int getMagicCode() {
        return AppSetting.getInteger(AppSetting.MAGIC_CODE);
    }
}
