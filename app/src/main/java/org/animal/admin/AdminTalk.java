package org.animal.admin;

import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class AdminTalk extends Talk {

    private static final String EXIT_CLASS_AND_METHOD = "intro.WelcomeTalk::welfare";

    private AuthService authService = AuthService.getInstance(talksConfig);

    public AdminTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("WelcomeTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("admin.AdminTalk::admin", InputTrigger.ofString("/admin")));
    }

    public TalkResponse admin(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Select one:"
           ).withButton("admin.AuthTalk::authorizations", "Authorizations")
            .withButton(EXIT_CLASS_AND_METHOD, "Main menu");
    }

    private TalkResponse unauthorizedResponse(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Not authorized."
           ).withPreserveHandlers();
    }
}
