package org.animal.geoloc;

import com.locationiq.client.model.Location;
import java.util.List;
import java.util.Optional;
import org.animal.AppSetting;
import org.animal.geoloc.GeolocMatches.GeolocMatch;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeolocService {

    private static Logger logger = LoggerFactory.getLogger(GeolocService.class);

    private static GeolocService instance;

    private final TalksConfiguration talksConfig;
    private final LocationIqClient locationIqClient;

    public static synchronized GeolocService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new GeolocService(talksConfig);
        }
        return instance;
    }

    private GeolocService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        LocationIqClient.initialize(AppSetting.get(AppSetting.LOCATION_IQ_API_KEY));
        locationIqClient = LocationIqClient.getInstance();
    }

    public GeolocMatches matches(String locationName) {
        GeolocMatches matches = new GeolocMatches();
        List<Location> locations = locationIqClient.forwardSearch(locationName);
        for (Location location : locations) {
            try {
                matches.locations.add(new GeolocMatch(
                    location.getDisplayName(),
                    location.getType(),
                    location.getLat(),
                    location.getLon()
                ));
            } catch (Exception e) {
                logger.warn("Geoloc: invalid coordinates with Location: " + location);
            }
        }
        return matches;
    }

    public Optional<GeolocMatch> select(GeolocMatches matches) {
        // returns the first match. A better approach could be implemented
        return matches.getSingleMatch().isPresent() ?
            matches.getSingleMatch() :
            matches.getMatch(0);
    }
}
