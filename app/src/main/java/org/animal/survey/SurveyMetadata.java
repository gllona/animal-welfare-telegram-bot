package org.animal.survey;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.logicas.librerias.talks.engine.TalkContext;

public interface SurveyMetadata {

    enum QuestionType {
        DISPLAY_ONLY,
        OPEN_TEXT,
        POSITIVE_INTEGER,
        SINGLE_SELECTION,
        MULTI_SELECTION,
        FINISH_COMPLETE,
        FINISH_INCOMPLETE
    }

    @AllArgsConstructor
    @ToString
    class Option {
        @Getter private String text;
        @Getter private String questionSlugToJump;
    }

    @AllArgsConstructor
    @EqualsAndHashCode(onlyExplicitlyIncluded = true)
    @ToString(onlyExplicitlyIncluded = true)
    class Question {
        @Getter @EqualsAndHashCode.Include @ToString.Include private String slug;
        @Getter private QuestionType type;
        @Getter private String text;
        @Getter private Option[] options;
        @Getter private TriFunction<String, TalkContext, Long, String> jumper;
    }

    Question[] getQuestions();
}
