package org.animal.survey;

import static org.animal.survey.SurveyMetadata.QuestionType.DISPLAY_ONLY;
import static org.animal.survey.SurveyMetadata.QuestionType.FINISH_COMPLETE;
import static org.animal.survey.SurveyMetadata.QuestionType.FINISH_INCOMPLETE;
import static org.animal.survey.SurveyMetadata.QuestionType.MULTI_SELECTION;
import static org.animal.survey.SurveyMetadata.QuestionType.OPEN_TEXT;
import static org.animal.survey.SurveyMetadata.QuestionType.POSITIVE_INTEGER;
import static org.animal.survey.SurveyMetadata.QuestionType.SINGLE_SELECTION;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.Pair;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.animal.Triggers;
import org.animal.communicator.CommunicatorService;
import org.animal.survey.SurveyMetadata.Option;
import org.animal.survey.SurveyMetadata.Question;

public class SurveyTalk extends Talk {

    private static final String CLIENT = "SURVEY_CLIENT";
    private static final String QUESTION = "SURVEY_QUESTION";
    private static final String VAR_NAME_PREFIX = "SURVEY_VAR_NAME_PREFIX";
    private static final String CONTINUE_AFTER_FINISH = "SURVEY_CONTINUE_AFTER_FINISH";
    private static final String ON_COMPLETE_FINISHING = "SURVEY_ON_COMPLETE_FINISH";
    private static final String ON_INCOMPLETE_FINISHING = "SURVEY_ON_INCOMPLETE_FINISH";
    private static final String PROCESS_ANSWER_METHOD = "survey.SurveyTalk::processAnswer";

    private final CommunicatorService communicatorService = CommunicatorService.getInstance(talksConfig);

    public SurveyTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("WelcomeTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    public TalkResponse start(
        TalkRequest request,
        String continueAfterFinishClassAndMethod,
        String clientFullClassName,
        String onIncompleteFinishCallbackMethodName,
        String onCompleteFinishCallbackMethodName,
        String varNamePrefix
    ) {
        getContext().set(request.getChatId(), VAR_NAME_PREFIX, varNamePrefix);
        getContext().set(request.getChatId(), CLIENT, clientFullClassName);
        getContext().set(request.getChatId(), ON_INCOMPLETE_FINISHING, onIncompleteFinishCallbackMethodName);
        getContext().set(request.getChatId(), ON_COMPLETE_FINISHING, onCompleteFinishCallbackMethodName);
        getContext().set(request.getChatId(), CONTINUE_AFTER_FINISH, continueAfterFinishClassAndMethod);

        SurveyMetadata metadata = getMetadata(request);
        if (metadata == null) {
            return invalidAccessResponse(request, "metadata is null");
        }

        String firstQuestionSlug = metadata.getQuestions()[0].getSlug();
        return renderQuestion(request, firstQuestionSlug);
    }

    private TalkResponse renderQuestion(TalkRequest request, String slug) {
        String clientClassName = getContext().getString(request.getChatId(), CLIENT);

        SurveyMetadata metadata = getMetadata(request);
        if (metadata == null) {
            return invalidAccessResponse(request, "metadata is null");
        }

        Question[] questions = metadata.getQuestions();
        Question question = getQuestion(questions, slug);
        if (question == null) {
            reportInvalidSlug(request, slug);
            return invalidAccessResponse(request, "question is null");
        }

        if (question.getType() == DISPLAY_ONLY) {
            renderText(request, question);
            Question nextQuestion = getNextQuestionInSequentialOrder(questions, question);
            if (nextQuestion == null) {
                reportInvalidEnd(request, question);
                nextQuestion = question;
            }
            return renderQuestion(request, nextQuestion.getSlug());
        }

        getContext().set(request.getChatId(), QUESTION, slug);

        return questionTalkResponse(request, question);
    }

    private TalkResponse questionTalkResponse(TalkRequest request, Question question) {
        String text = question.getText();
        if (text == null || text.trim().isEmpty()) {
            reportEmptyQuestion(request, question);
            text = "(Continue)";
        }

        TalkResponse response = TalkResponse.ofText(request.getChatId(), text);
        attachHandlers(request, response, question);
        return response;
    }

    private void attachHandlers(TalkRequest request, TalkResponse response, Question question) {
        String nextMethod = question.getType() == FINISH_COMPLETE || question.getType() == FINISH_INCOMPLETE ?
            getContext().getString(request.getChatId(), CONTINUE_AFTER_FINISH) :
            PROCESS_ANSWER_METHOD;

        if (question.getType() == SINGLE_SELECTION) {
            for (Option option : question.getOptions()) {
                if (option.getText() == null || option.getText().trim().isEmpty()) {
                    continue;
                }
                response.withButton(nextMethod, option.getText());
            }
        } if (question.getType() == POSITIVE_INTEGER) {
            response.withRegex(nextMethod, Triggers.POSITIVE_INTEGER);
        } else if (question.getType() == OPEN_TEXT) {
            response.withRegex(nextMethod, Triggers.NOT_A_COMMAND);
        } else if (question.getType() == MULTI_SELECTION) {
            reportUnsupportedQuestionType(request, question);
        }
    }

    private void renderText(TalkRequest request, Question question) {
        if (question.getText() == null || question.getText().trim().isEmpty()) {
            return;
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                request.getChatId(),
                question.getText()
            ).withPreserveHandlers()
        );
    }

    public TalkResponse processAnswer(TalkRequest request) {
        String clientClassName = getContext().getString(request.getChatId(), CLIENT);
        String questionSlug = getContext().getString(request.getChatId(), QUESTION);

        SurveyMetadata metadata = getMetadata(request);
        if (metadata == null) {
            return invalidAccessResponse(request, "metadata is null");
        }

        // input for the question

        Question[] questions = metadata.getQuestions();
        Question question = getQuestion(questions, questionSlug);

        String input = request.getMatch().getRegexMatches().get(0);

        getContext().set(request.getChatId(), getQuestionVarName(request, questionSlug), input);

        // response

        Question nextQuestion = getNextQuestion(questions, question, input, request.getChatId());

        if (nextQuestion == null) {
            reportNextQuestionIsNull(request, question, input);
            return invalidAccessResponse(request, "question's next question is undefined");
        }

        if (nextQuestion.getType() == FINISH_COMPLETE) {
            callOnFinishCompleteCallback(request);   // return value is not used
        } else if (nextQuestion.getType() == FINISH_INCOMPLETE) {
            callOnFinishIncompleteCallback(request);   // return value is not used
        }

        return renderQuestion(request, nextQuestion.getSlug());
    }

    private TalkResponse callOnFinishIncompleteCallback(TalkRequest request) {
        return callOnFinishCallback(request, ON_INCOMPLETE_FINISHING);
    }

    private TalkResponse callOnFinishCompleteCallback(TalkRequest request) {
        return callOnFinishCallback(request, ON_COMPLETE_FINISHING);
    }

    private TalkResponse callOnFinishCallback(TalkRequest request, String finishType) {
        Map<Pair<String, String>, String> answers = gatherAnswers(request);

        String finishMethodName = getContext().getString(request.getChatId(), finishType);
        Method m = getCallBackMethod(request, finishMethodName);
        if (m == null) {
            return genericFinishResponse(request);
        }

        Class<Talk> clientTalkClass = getClientTalkClass(request);
        if (clientTalkClass == null) {
            return genericFinishResponse(request);
        }

        Talk callbackTalk = talkForClass(clientTalkClass);

        TalkResponse response;
        try {
            response = (TalkResponse) m.invoke(callbackTalk, request, answers);
        } catch (IllegalAccessException | InvocationTargetException e) {
            reportCanNotInvokeClientMethod(request, clientTalkClass.getSimpleName(), finishMethodName);
            return invalidAccessResponse(request, "Can not invoke callback method");
        }

        deleteAnswersFromContext(request);

        return response;
    }

    private Method getCallBackMethod(TalkRequest request, String callbackMethodName) {
        Class<Talk> clientTalkClass = getClientTalkClass(request);
        if (clientTalkClass == null) {
            return null;
        }

        try {
            return clientTalkClass.getMethod(callbackMethodName, TalkRequest.class, Map.class);
        } catch (NoSuchMethodException e) {
            reportClientMethodNotFound(request, clientTalkClass.getSimpleName(), callbackMethodName);
            return null;
        }
    }

    private Class<Talk> getClientTalkClass(TalkRequest request) {
        String clientClassName = getContext().getString(request.getChatId(), CLIENT);
        try {
            return (Class<Talk>) Class.forName(clientClassName);
        } catch (ClassNotFoundException e) {
            reportClientTalkClassNotFound(request, clientClassName);
            return null;
        }
    }

    private Map<Pair<String, String>, String> gatherAnswers(TalkRequest request) {
        String varNamePrefix = getContext().getString(request.getChatId(), VAR_NAME_PREFIX);

        SurveyMetadata metadata = getMetadata(request);
        if (metadata == null) {
            reportMetadataNotFound(request);
            throw new RuntimeException("metadata is null");
        }
        Question[] questions = metadata.getQuestions();

        Map<String, Object> vars = getContext().getAll(request.getChatId());

        Map<Pair<String, String>, String> answers = Stream.of(questions)
            .filter(q -> q.getType() != DISPLAY_ONLY && q.getType() != FINISH_COMPLETE && q.getType() != FINISH_INCOMPLETE)
            .collect(Collectors.toMap(
                q -> Pair.of(q.getSlug(), q.getText()),
                q -> {
                    String answer = (String) vars.get(varNamePrefix + q.getSlug());
                    return answer == null ? "N/A" : answer;
                }
            ));

        return answers;
    }

    private void deleteAnswersFromContext(TalkRequest request) {
        String varNamePrefix = getContext().getString(request.getChatId(), VAR_NAME_PREFIX);

        getContext().getAll(request.getChatId())
                    .keySet().stream()
                    .filter(slug -> slug.startsWith(varNamePrefix))
                    .forEach(slug -> getContext().remove(request.getChatId(), slug));
    }

    private String getQuestionVarName(TalkRequest request, String slug) {
        String varNamePrefix = getContext().getString(request.getChatId(), VAR_NAME_PREFIX);
        return varNamePrefix + slug;
    }


    private Question getQuestion(Question[] questions, String slug) {
        return Stream.of(questions)
                     .filter(q -> q.getSlug().equals(slug))
                     .findAny()
                     .orElse(null);
    }

    private Question getNextQuestionInSequentialOrder(Question[] questions, Question question) {
        boolean found = false;
        for (Question q : questions) {
            if (q.equals(question)) {
                found = true;
                continue;
            }
            if (found) {
                return q;
            }
        }

        return null;
    }

    private Question getNextQuestion(Question[] questions, Question question, String input, long sessionId) {
        String nextSlug = null;

        Option[] options = question.getOptions();
        if (options != null && options.length > 0) {
            Option option = Stream.of(options)
                                  .filter(o -> input.equals(o.getText()))
                                  .findFirst()
                                  .orElse(null);
            if (option != null && option.getQuestionSlugToJump() != null) {
                nextSlug = option.getQuestionSlugToJump();
            }
        }

        if (question.getJumper() != null) {
            nextSlug = question.getJumper().apply(input, context, sessionId);
        }

        if (nextSlug != null) {
            return getQuestion(questions, nextSlug);
        }

        return getNextQuestionInSequentialOrder(questions, question);
    }

    private SurveyMetadata getMetadata(TalkRequest request) {
        String clientClassName = getContext().getString(request.getChatId(), CLIENT);
        if (clientClassName == null) {
            return null;
        }

        Object result;
        try {
            Class<?> cls = Class.forName(clientClassName);
            Method m = cls.getMethod("getSurveyMetadata");
            result = m.invoke(null);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Can not retrieve survey metadata from class " + clientClassName, e);
        }

        return (SurveyMetadata) result;
    }

    private TalkResponse invalidAccessResponse(TalkRequest request, String from) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Invalid access " + (from == null ? "" : "(" + from + ")") + ".\n"
                + "\n"
                + "* Restart the bot: /start"
        );
    }

    private TalkResponse genericFinishResponse(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Survey finished.\n"
                + "\n"
                + "* Restart the bot: /start"
        );
    }

    private void reportInvalidSlug(TalkRequest request, String slug) {
        communicatorService.sendToAdmins(request, "Survey: invalid slug " + slug);
    }

    private void reportEmptyQuestion(TalkRequest request, Question question) {
        communicatorService.sendToAdmins(request, "Survey: empty question " + question.getSlug());
    }

    private void reportUnsupportedQuestionType(TalkRequest request, Question question) {
        communicatorService.sendToAdmins(request, "Survey: unsupported question type for " + question.getSlug());
    }

    private void reportInvalidEnd(TalkRequest request, Question question) {
        communicatorService.sendToAdmins(request, "Survey: invalid end in question " + question.getSlug());
    }

    private void reportNextQuestionIsNull(TalkRequest request, Question question, String input) {
        communicatorService.sendToAdmins(request, "Survey: next question to " + question.getSlug() + " with input [" + input + "] is null");
    }

    private void reportClientTalkClassNotFound(TalkRequest request, String clientClassName) {
        communicatorService.sendToAdmins(request, "Survey: client talk class not found " + clientClassName);
    }

    private void reportClientMethodNotFound(TalkRequest request, String talkClassName, String callbackMethodName) {
        communicatorService.sendToAdmins(request, "Survey: client method not found " + talkClassName + "#" + callbackMethodName);
    }

    private void reportCanNotInvokeClientMethod(TalkRequest request, String talkClassName, String callbackMethodName) {
        communicatorService.sendToAdmins(request, "Survey: can not invoke client method " + talkClassName + "#" + callbackMethodName);
    }

    private void reportMetadataNotFound(TalkRequest request) {
        communicatorService.sendToAdmins(request, "Survey: can not get metadata");
    }
}
