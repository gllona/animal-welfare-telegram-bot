package org.animal.earth;

import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.animal.Place;
import org.animal.PlaceMainTalk;

public class EarthTalk extends PlaceMainTalk {

    public EarthTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("WelcomeTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("earth.EarthTalk::menu", InputTrigger.ofString("/earth")));
    }

    public TalkResponse menu(TalkRequest request) {
        if (! placeMatches(request)) {
            return TalkResponse.ofText(
                request.getChatId(),
                "Please restart the bot: /start"
            );
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Do you want to report animal cruelty?"
        ).withButton("earth.EarthTalk::startReport", "Yes")
         .withButton("earth.EarthTalk::dontReport", "No");
    }

    public TalkResponse dontReport(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "This bot allows you to report animal cruelty. In the future, to check other bot capabilities, please tap on /start"
        );
    }

    public TalkResponse startReport(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Choose one:"
        ).withButton("earth.EarthTalk::reportFromNewYorkCity", "I'm in New York City")
         .withButton("earth.EarthTalk::reportFromAnywhereInTheUS", "I'm in other areas of the U.S.")
         .withButton("earth.EarthTalk::reportFromOtherCountry", "I'm in other country different than the U.S.")
         .withButton("earth.EarthTalk::reportOnline", "I want to submit a report online");
    }

    public TalkResponse reportFromNewYorkCity(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "* If you need to report animal cruelty, please contact 311.\n"
                + "* To report crimes in progress in any borough, please call 911.\n"
                + "\n"
                + "* Go to /menu"
        ).withString("earth.EarthTalk::menu", "/menu");
    }

    public TalkResponse reportFromAnywhereInTheUS(TalkRequest request) {
        return TalkResponse.ofHtml(
            request.getChatId(),
            "* Find out who is responsible for investigating and enforcing the anti-cruelty codes in your town, county and/or state, "
                    + "such as your local humane organization, animal control agency, taxpayer-funded animal shelter or police precinct.\n"
                + "* If you have trouble finding the correct agency to contact, call or visit your local police department or "
                    + "<a href=\"https://www.aspca.org/adopt-pet/find-shelter\">your local shelter or animal control agency</a> for assistance.\n"
                + "\n"
                + "* Go to /menu"
        ).withString("earth.EarthTalk::menu", "/menu");
    }

    public TalkResponse reportFromOtherCountry(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "* Find out who are responsible for enforcing the anti-cruelty codes in your country and contact them.\n"
                + "\n"
                + "* Go to /menu"
        ).withString("earth.EarthTalk::menu", "/menu");
    }

    public TalkResponse reportOnline(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "You will be asked some questions about about the case. At the end you will have the option to submit the report anonymously or "
                + "to provide your identification and contact information."
        ).withButton("earth.EarthTalk::whereDidYouSeeIt", "Continue");
    }

    public TalkResponse whereDidYouSeeIt(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Where you see the animal abuse?"
        ).withButton("earth.EarthTalk::sawItOnline", "Online or in TV")
         .withButton("earth.EarthSurveyTalk::start", "I witnessed the abuse");
    }

    public TalkResponse sawItOnline(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "* For directions please check out 📝 https://www.aspca.org/take-action/report-animal-cruelty\n"
                + "\n"
                + "* Go to /menu"
        ).withString("earth.EarthTalk::menu", "/menu");
    }

    private boolean placeMatches(TalkRequest request) {
        String country = getContext().getString(request.getChatId(), "PLACE");
        return Place.EARTH.name().equals(country);
    }
}
