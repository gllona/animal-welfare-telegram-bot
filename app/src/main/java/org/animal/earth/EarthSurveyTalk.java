package org.animal.earth;

import static org.animal.survey.SurveyMetadata.QuestionType.DISPLAY_ONLY;
import static org.animal.survey.SurveyMetadata.QuestionType.FINISH_COMPLETE;
import static org.animal.survey.SurveyMetadata.QuestionType.OPEN_TEXT;
import static org.animal.survey.SurveyMetadata.QuestionType.POSITIVE_INTEGER;
import static org.animal.survey.SurveyMetadata.QuestionType.SINGLE_SELECTION;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.Pair;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.animal.Place;
import org.animal.communicator.CommunicatorService;
import org.animal.survey.SurveyMetadata;
import org.animal.survey.SurveyMetadata.Option;
import org.animal.survey.SurveyMetadata.Question;
import org.animal.survey.SurveyTalk;

public class EarthSurveyTalk extends Talk {

    private static final String VAR_NAME_PREFIX = "SURVEY_EARTH_";

    private final CommunicatorService communicatorService = CommunicatorService.getInstance(talksConfig);

    public EarthSurveyTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("WelcomeTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    public TalkResponse start(TalkRequest request) {
        return talkForClass(SurveyTalk.class)
            .start(
                request,
                "earth.EarthSurveyTalk::continueAfterFinish",
                getClass().getCanonicalName(),
                "partialSurveyCallback",
                "completeSurveyCallback",
                VAR_NAME_PREFIX
            );
    }

    public TalkResponse continueAfterFinish(TalkRequest request) {
        // do nothing, a "Continue" button is not defined
        return null;
    }

    public TalkResponse partialSurveyCallback(TalkRequest request, Map<Pair<String, String>, String> answers) {
        return surveyCallback(request, answers, "partial answers");
    }

    public TalkResponse completeSurveyCallback(TalkRequest request, Map<Pair<String, String>, String> answers) {
        return surveyCallback(request, answers, "COMPLETE ANSWERS");
    }

    private TalkResponse surveyCallback(TalkRequest request, Map<Pair<String, String>, String> answers, String completionState) {
        String answersText =
            "username: " + (request.getMessage().chat().username() == null ? "N/A" : "@" + request.getMessage().chat().username()) + "\n" +
            "name: " + (request.getMessage().chat().firstName() +
                (request.getMessage().chat().lastName() == null ? "" : " " + request.getMessage().chat().lastName())) + "\n";
        answersText += answers.keySet().stream()
            .sorted()
            .map(pair -> {
                String slug = pair.getLeft();
                String question = pair.getRight();
                String answer = answers.get(pair);
                return "* " + slug + " = " + answer + " <-- " + shortenQuestion(question);
            })
            .collect(Collectors.joining("\n"));

        String tgUsername = request.getMessage().chat().username();
        String userInfo = request.getMessage().chat().firstName() +
            (request.getMessage().chat().lastName() == null ? "" : " " + request.getMessage().chat().lastName()) +
            (tgUsername == null ? "" : " (@" + tgUsername + ")");

        communicatorService.sendToOperatorsUnboxed(
            request,
            Place.EARTH,
            "=== SURVEY ===\n"
                + "(" + completionState + ") by " + userInfo,
            answersText,
            "=== END OF SURVEY ===",
            "To answer: /answer_" + request.getChatId() + (tgUsername == null ? "" : "_" + tgUsername)
        );

        return TalkResponse.ofText(   // this return value is not used at all
            request.getChatId(),
            "Done!"
        );
    }

    private String shortenQuestion(String text) {
        return text == null ?
            null :
            (text.length() > 40 ? text.substring(0, 36) + "...." : text).replace("\n", " - ");
    }

    public static SurveyMetadata getSurveyMetadata() {
        Question[] questions = new Question[]{

            new Question(
                "intro",
                DISPLAY_ONLY,
                "You can write any amount of information as you want, but please don't send images, audios or videos. Only text is supported.\n"
                    + "\n"
                    + "In case you don't want to continue providing information, come back here and tap on /earth. "
                        + "But remember that we need all the answers to help with your case.",
                null,
                null
            ),

            new Question(
                "physical_signs_of_cruelty",
                DISPLAY_ONLY,
                "PHYSICAL SIGNS OF CRUELTY",
                null,
                null
            ),

            new Question(
                "01",
                SINGLE_SELECTION,
                "Does the animal have a tight collar that has caused a neck wound or has become embedded in the pet's neck?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "02",
                SINGLE_SELECTION,
                "Does the animal have open wounds, signs of multiple healed wounds or an ongoing injury or illness that isn't being treated?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "03",
                SINGLE_SELECTION,
                "Does the animal have an untreated skin conditions that have caused loss of hair, scaly skin, bumps or rashes?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "04",
                SINGLE_SELECTION,
                "Does the animal show extreme thinness or are emaciation—bones visible?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "05",
                SINGLE_SELECTION,
                "Does the animal have fur infested with fleas, ticks or other parasites?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "06",
                SINGLE_SELECTION,
                "Does the animal have patches of bumpy, scaly skin rashes?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "07",
                SINGLE_SELECTION,
                "Does the animal show signs of inadequate grooming, such as extreme matting of fur, overgrown nails and dirty coat?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "08",
                SINGLE_SELECTION,
                "Does the animal show weakness, limping or the inability to stand or walk normally?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "09",
                SINGLE_SELECTION,
                "Does the animal have heavy discharge from eyes or nose?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "10",
                SINGLE_SELECTION,
                "Is there an owner striking or otherwise physically abusing the animal?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "11",
                SINGLE_SELECTION,
                "Does the animal show visible signs of confusion or extreme drowsiness?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "environmental_signs_of_cruelty",
                DISPLAY_ONLY,
                "ENVIRONMENTAL SIGNS OF CRUELTY",
                null,
                null
            ),

            new Question(
                "20",
                SINGLE_SELECTION,
                "Is the animal tied up alone outside for long periods of time without adequate food or water, or with food or water that is unsanitary?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "21",
                SINGLE_SELECTION,
                "Is the animal kept outside in inclement weather without access to adequate shelter?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "22",
                SINGLE_SELECTION,
                "Is the animal kept in an area littered with feces, garbage, broken glass or other objects that could harm them?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "23",
                SINGLE_SELECTION,
                "Is the animal housed in kennels or cages (very often crowded in with other animals) that are too small to allow them to stand, turn around and make normal movements?",
                new Option[]{
                    new Option("Yes", null),
                    new Option("No", null),
                    new Option("Don't know", null)
                },
                null
            ),

            new Question(
                "additional_information",
                DISPLAY_ONLY,
                "ADDITIONAL INFORMATION",
                null,
                null
            ),

            new Question(
                "30",
                OPEN_TEXT,
                "When the animal abuse occurred? Write the answer in a single paragraph.",
                null,
                null
            ),

            new Question(
                "31",
                SINGLE_SELECTION,
                "Can you provide names and contact information of other people who have firsthand information about the abusive situation?",
                new Option[]{
                    new Option("Yes", "31a"),
                    new Option("No", "32")
                },
                null
            ),

            new Question(
                "31a",
                OPEN_TEXT,
                "Write their names and contact information in a single paragraph:",
                null,
                null
            ),

            new Question(
                "32",
                OPEN_TEXT,
                "Add any other information that is relevant in a single paragraph:",
                null,
                null
            ),

            new Question(
                "submit_the_report",
                DISPLAY_ONLY,
                "SUBMIT THE REPORT",
                null,
                null
            ),

            new Question(
                "40",
                SINGLE_SELECTION,
                "Do you want to submit your report anonymously?",
                new Option[]{
                    new Option("Yes", "41"),
                    new Option("No", "40a")
                },
                null
            ),

            new Question(
                "40a",
                OPEN_TEXT,
                "Your complete name:",
                null,
                null
            ),

            new Question(
                "40b",
                OPEN_TEXT,
                "Your telephone number:",
                null,
                null
            ),

            new Question(
                "40c",
                OPEN_TEXT,
                "Your e-mail address:",
                null,
                null
            ),

            new Question(
                "40d",
                POSITIVE_INTEGER,
                "What's your age? Enter a number:",
                null,
                (input, context, sessionId) -> {
                    // use jumpers to add more complex conditional logic. Return a question slug for each option you have
                    long age = Long.parseLong(input);
                    if (age < 16) {
                        // you can ask additional questions (not done here)
                        return "41";
                    } else {
                        return "41";
                    }
                }
            ),

            new Question(
                "41",
                SINGLE_SELECTION,
                "Press \"Submit\" to submit your report.",
                new Option[]{
                    new Option("Submit", null)
                },
                null
            ),

            new Question(
                "42",
                FINISH_COMPLETE,   // use FINISH_INCOMPLETE in any question to invoke partialSurveyCallback() instead of completeSurveyCallback()
                "💚 Thank you! You will be contacted soon.\n"
                    + "\n"
                    + "Pay attention to Telegram!\n"
                    + "\n"
                    + "Tap on /earth to go back to the main menu.",
                null,
                null
            )
        };

        return () -> questions;
    }
}
