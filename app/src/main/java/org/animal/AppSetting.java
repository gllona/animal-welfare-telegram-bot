package org.animal;

public enum AppSetting {
    HELLO_WORLD,
    FLYWAY_URL,
    FLYWAY_USER,
    FLYWAY_PASSWORD,
    DB_RDBMS,
    DB_HOST,
    DB_PORT,
    DB_DATABASE,
    DB_USERNAME,
    DB_PASSWORD,
    DB_OPTIONS,
    DB_CONNECTION_POOL_SIZE,
    DB_CONNECTION_POOL_TIMEOUT_IN_MILLIS,
    DB_CONNECTION_TTL_IN_MILLIS,
    DB_KEEPALIVE_QUERY,
    LOCATION_IQ_API_KEY,
    MAGIC_CODE;

    public static String get(AppSetting setting) {
        return AppConfig.getInstance().getString(setting);
    }

    public static int getInteger(AppSetting setting) {
        return Integer.parseInt(get(setting));
    }
}
